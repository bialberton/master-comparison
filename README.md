# master-comparison

This project was developed to apply the correction for the multiplicity of tests across contrasts, and to evaluate the family-wise error rate and power from each method, which are the following:

* Permutation (Westfall-Young)
* Dunn–Šidák
* Bonferroni
* Fisher's LSD
* Tukey (this methods has various names, and perhaps the most known is HSD)
* Scheffé
* Fisher-Hayter
* Wang-Cui

The main reference for this project is the preprint in bioRxiv:  https://doi.org/10.1101/775106 

The codes available here can be run in MATLAB or Octave. To apply the correction, you must install:
* PALM - Permutation Analysis of Linear Models (available at https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PALM)
* The R Project for Statistical Computing (available at https://www.r-project.org/)

To run the simulations, you can set the variables and run function **corrcon_loop.m**

To evaluate the statistical tests in any image, use the function **statTests.m** (note that the functions were originally written to be used with **corrcon_loop.m**, but you can adapt them to your own applications).

There are also some scripts in the folder *tests* that show some nice properties of some methods and how to add signal to simulations.
