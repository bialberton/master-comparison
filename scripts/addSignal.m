function [Y] = addSignal(Y, M, C, nS, alpha,dist)
% Function that adds signal to the first 2 groups so that A-B = 50% of
% power
%
% Usage:
% Y = addSignal(Y, M, C, nS, alpha)
% 
% Inputs: 
% Y    : n by v matrix with noise data, where n is the number of subjects
%        and v the number of voxels.
% C    : nG by nC matrix with the contrasts, where nG is the number of
%        groups and nC is the the number of contrasts
% M    : n by nG matrix with the design matrix
% nS   : nG x 1 array of the number of subjects in each group
% alpha: significance level of the t test
% dist : distribution of data. 
%        Options: norm - normal distribution
%                 weib - weibull distribution
%                 lapl - laplace distribution
%
% Outputs:
% Y   : n by m matrix with signal added to the first 2 groups
%
if (dist == 'weib')
    sigFactor = 5/8; %Signal Factor to get 50% power (before it was 0.725)
elseif (dist == 'lapl')
    sigFactor = 0.98; %Signal Factor to get 50% power
else
    sigFactor = 1;   %Signal Factor to get 50% power
end    

[N, V]  = size(Y);
df      = N-rank(M);
effi    = sqrt(C(:,1)'*pinv(M'*M)*C(:,1));       %efficiency
a       = sigFactor* tinv(1-alpha,df) * effi/2;  %signal factor

%Adding signal to the matrix
Y(1:nS(1),:)                 = Y(1:nS(1),:)+ a*ones(nS(1),V);                  %add signal to the 1st group
Y((nS(1)+1):(nS(1)+nS(2)),:) = Y((nS(1)+1):(nS(1)+nS(2)),:) - a*ones(nS(2),V); %subtract the signal to the 2nd group


