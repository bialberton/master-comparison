clear; close all;
% indir  = aux;
indir = '/home/bianca/Documents/Mestrado/ContrastCorrection/syntheticData/run03/weib_dist';
outdir = indir;
% plotsdir = 'errorbars';
plotsdir = 'errorbars_perm';

alpha       = 0.05;      % test level
V           = 2000;      % num of voxels
nP          = 2500;
ns          = 10;        %number of subjects per group
dist        = 'weib';    % Options: 'norm','weib' and 'lapl'

% Groups tested
nGtested     = [4,5,7,9];
nGtotal     = size(nGtested,2);

 % Strings to be printed in the csv file
test     = {"T para.     ", "T perm      ","Perm Corrected", "Sidak para. ","Sidak perm. ", "LSD para.   ","LSD perm.   ", "Tukey       ", "Hayter      ", "Wang-Cui","Scheffe-no F", "Bonferroni"};
typeError= {   "PCER",      "PCER",          "FWER",             "FWER",      "FWER",         "FWER",        "FWER",        "FWER",         "FWER",       "FWER",       "FWER"  ,       "FWER" };
ntypeTests = size(test,2);

%find the folders with data
files = dir(indir);
dirFlags = [files.isdir];
folders = files(dirFlags);
nFolders = size(folders,1);

if(exist(outdir,'dir') ~= 7)
    mkdir(outdir);
end

plotsdir = fullfile(outdir,plotsdir);
if(exist(plotsdir,'dir') ~= 7)
    mkdir(plotsdir);
end

fid = fopen( fullfile(outdir,'TestsResults.csv'),'w');
%fid =1
% tic;
for i = 1 : nFolders
    folderName = folders(i).name;
    %Checks if the folder has the data => the first string on the folder
    %name is ortho
    if(contains(folderName,'ortho'))
    %if(contains(folderName,'signal-1'))
        subset = contains(folderName,'ortho-true')||contains(folderName,'ortho-1');
        balance = contains(folderName,'balance-true')||contains(folderName,'balance-1');
        withSignal = contains(folderName,'signal-true')||contains(folderName,'signal-1');
        fprintf(1,'folderName: %s , subset: %d , balance: %d, withSignal: %d\n', folderName, subset,balance, withSignal)
        
        thisDir = fullfile(indir, folderName);
        
        erT1toNG    = zeros(nGtotal,ntypeTests);
        erT3toNG    = zeros(nGtotal,ntypeTests);
        erT1std     = zeros(nGtotal,ntypeTests);
        erT3std     = zeros(nGtotal,ntypeTests);
        erT1Interval= zeros(nGtotal,2,ntypeTests);
        erT3Interval= zeros(nGtotal,2,ntypeTests);
        ci          = zeros(nGtotal,2,ntypeTests);
        meanPower   = zeros(nGtotal,ntypeTests);
        
        %Concatenate files and calculate the confidence interval
       for nGi = 1 : nGtotal
            %N = 8*nG;
            %Check if there was any file with all concatenated error/power and delete it
            if(size(dir(fullfile(outdir,'all*.csv')),1) ~= 0)
                command = ['rm ',outdir,'/','all*.csv'];
                system(command);
            end
            
            %Concatenate all csv files and save the mean of error rate and power
            %Error rate
            
            dataCSV = sprintf('%sDist_%03dgroups',dist,nGtested(nGi));
            concatfile = sprintf('allEr1toNG_%01d%01d%01d%s.csv',subset,balance,withSignal,dataCSV);
            command = sprintf('cat %s/erT1toNG_%s_*.csv >> %s/%s', thisDir, dataCSV, outdir, concatfile);
            system(command);
            
            er = load(fullfile(outdir,concatfile));
            erT1toNG(nGi,:) = mean(er,1);
            erT1Interval(nGi,1,:) = min(er);
            erT1Interval(nGi,2,:) = max(er);
            erT1std(nGi,:) = std(er);
            
            concatfile = sprintf('allEr3toNG_%01d%01d%01d%s.csv',subset,balance,withSignal,dataCSV);
            command = sprintf('cat %s/erT3toNG_%s_*.csv >> %s/%s', thisDir, dataCSV, outdir, concatfile);
            system(command);
            
            er = load(fullfile(outdir,concatfile));
            erT3toNG(nGi,:) = mean(er,1);
            erT3Interval(nGi,1,:) = min(er);
            erT3Interval(nGi,2,:) = max(er);
            erT3std(nGi,:) = std(er);
            nTests = size(er,1);
                        
            %Power
            concatfile = sprintf('allPower_%01d%01d%01d%s.csv',subset,balance,withSignal,dataCSV);
            command = sprintf('cat %s/power_%s_*.csv >> %s/%s', thisDir, dataCSV, outdir, concatfile);
            system(command);
            
            power = load(fullfile(outdir,concatfile));
            meanPower(nGi,:) = mean(power,1);
            %     fileName = ['mean_',concatfile];
            %     dlmwrite(fullfile(outdir,fileName), meanPower, 'precision',10);
            
            if(withSignal)
                [ci(nGi,1,:), ci(nGi,2,:)] = confint(nTests*V, erT3toNG(nGi,:) * nTests *V, alpha, 'Wilson');
                erMaxMin = erT3Interval;
                erStd = erT3std;
            else
                [ci(nGi,1,:), ci(nGi,2,:)] = confint(nTests*V, erT1toNG(nGi,:) * nTests *V, alpha, 'Wilson');
                erMaxMin = erT1Interval;
                erStd = erT1std;
            end
        end
        
%         % Print the data - if you wish to print to a .csv file, uncomment line
%         fprintf(fid, "Parameters:| V: %i | nP: %i | Distribution:| %s | is subset?:| %i | is balanced?:| %i | has signal?:| %i |\n\n\n",V,nP,dist,subset,balance,withSignal);
%         fprintf(fid, " Teste        | ER   |");
%         for i = 1 : nGtotal
%             tableTitle    = {sprintf("ER 1 to %2d",nGtested(i)), sprintf("ER 3 to %2d",nGtested(i)), "L CI    ", "U CI    ", "Power   "};
%             fprintf(fid, " | %s | %s | %s | %s | %s |",tableTitle{:} );
%         end
%         fprintf(fid,"\n");
%         
%         for i=1: size(power,2)
%             fprintf(fid, '  %s | %s |', test{i}, typeError{i});
%             for j = 1 : nGtotal
%                 fprintf(fid, ' | %f   | %f   | %f | %f | %f |',  erT1toNG(j, i), erT3toNG(j, i), ci(j, 1, i),ci(j, 2, i), meanPower(j, i));
%             end
%             fprintf(fid,"\n");
%         end
%         
%         fprintf(fid,"\n\n\n");
        
        graphPlots
    end
end
% toc
if fid ~= 1
    fclose( fid );
end

close all