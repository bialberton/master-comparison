function corrcon_func(seed,datadir,outdir,withSignal,V,N,nG,nP,alpha,dist,balance,cSubset, MCsample)
% Function that creates simulated data, calculates the error rate and print
% it to three csvs files: 
%       - erT1toNG_*.csv = file with the error considering all contrasts
%       - erT3toNG_*.csv = file with the error of all contrasts except
%         those which have signal in it (all contrasts related to groups 1 and 2)
%       - power_*.csv    = file with the power of test (calculated in relation to the first contrast)    
%
% Usage:
% corrcon_func(seed,outdir,withSignal,V,N,nG,nP,alpha,dist,balance, cOrthogonal)
% 
% Inputs: 
% seed        : id of the actual run. Leave it 0 if you dont need to run the code
%               multiple times
% datadir     : folder where the R values will be stored/retrieved
% outdir      : folder where the csv files will be stored
% withsignal  : flag that chooses if this experiment will have signal in
%                 it (the signal is added to groups 1 and 2). Use true if
%                 there will be signal and false otherwise
% V           : number of voxels
% N           : number of subjects
% nG          : number of groups
% nP          : number of permutations of the permutation test
% alpha       : significance level of the test (e.g. 0.05%)
% dist        : distribution used in the data. Can be 'norm' (normal), 'weib'
%               (Weibull distribution) or 'lapl' (laplace dist.)
% balance     : flag that indicates if this model is balanced. Use true if
%               there is the same number of subjects in each group and false
%               otherwise
% cSubset     : indicates if all contrasts will be used or only a subset of 
%               l.i. orthogonal contrasts will be used. Use true if contrasts
%               the difference between all groups in relation to the 3th
%               group (that has no signal) or false if all contrasts will
%               be used.
% MCsample   : number of monte Carlo Samples used in the Wang-Cui method
% 

%%%%%%%%%%%%%%%%%%%%%%%%% Experiment's Data / Tests %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%M and Cset
[M, Cset, nS] = expGenerator(N, nG, balance);

%Y
Y = dataGenerator(N,V,dist);

CpowerIdx = false(1,size(Cset,2));
if(withSignal)
    Y = addSignal(Y, M, Cset, nS, alpha,dist);
    CpowerIdx =  any(Cset(1:2,:),1); %contrasts without signal in it - involving groups 1 and 2
end

%Uses a subset of orthogonal constrasts that doesn't have signal in all
%contrasts 
if(cSubset) 
   Cset   = Cset(:,((nG-1)*2+1):(nG-1)*3);
   CpowerIdx = CpowerIdx(((nG-1)*2+1):(nG-1)*3);
end


%Calculates all tests
[erT1toNG, erT3toNG, power, ~ ] = statTests(Y,M,Cset,V,N,nS,nG,nP,alpha, datadir,  outdir, CpowerIdx, MCsample);

%%%%%%%%%%%%%%%%%%%%%%% Print data %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Error considering the contrasts in all groups (from group 1 to nG)
fileName = sprintf('erT1toNG_%sDist_%03dgroups_seed%05d.csv',dist,nG,seed);
dlmwrite(fullfile(outdir,fileName), erT1toNG, 'precision',20);

%if(withSignal)
    %Error considering the groups without signal (that doesn't include groups 1 and 2)
    fileName = sprintf('erT3toNG_%sDist_%03dgroups_seed%05d.csv',dist,nG,seed);
    dlmwrite(fullfile(outdir,fileName), erT3toNG, 'precision',20);

    %Power of the test
    fileName = sprintf('power_%sDist_%03dgroups_seed%05d.csv',dist,nG,seed);
    dlmwrite(fullfile(outdir,fileName), power, 'precision',20);
%end
