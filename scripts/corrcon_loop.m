clear;

%Parameters
alpha       = 0.05;      % test level
V           = 2000;      % num of voxels
%nG          = 15;         % num of groups
nGroups     = [3,4,5,7,9]; % Group sizes
nP          = 5000;        % Number of permutations
MCsample    = nP;         % Number of Monte Carlo samples used in the Wang-Cui test
numRuns     = 5000;         % The number of runs and permutations are always the same
dist        = 'weib';    % Options: 'norm','weib' and 'lapl'
balance     = false;     % unbalanced model
cSubset     = false;
withSignal  = true;

%Restores the seed to default - only in MATLAB
%rng('default');


for ns = 10 : 10
    aux = sprintf('results/test%02dsubjects_%s', ns,dist);
    
    for cSubset = 0 : 1
        for withSignal = 0 : 1
            for balance = 0 : 1
                
                outdir = sprintf('%s/ortho-%d-signal-%d-balance-%d',aux,cSubset,withSignal,balance);
                if(exist(outdir,'dir') ~= 7)
                    mkdir(outdir);
                end
                
                %Loop
                for nG = nGroups
                    N = ns * nG; %In average, the groups are going to have 8 subjects per group
                    nG
                    for seed = 1 : numRuns
%                         seed
                        %         %In MATLAB:
                        %         rng('shuffle');
                        %
                        %         %In OCTAVE:
                        %         rand("seed","reset");
                        tic;
                        corrcon_func(seed,outdir,outdir,withSignal,V,N,nG,nP,alpha,dist,balance,cSubset,MCsample)
                        toc
                    end
                end
            end
        end
    end
    
    concatCsv_script
end

