%Script used to create all R files that we're going to need to run in the
%cluster
clear;

alpha=0.05;
outdir = [pwd,'/results'];

for nG = 5 : 5 : 15
    N = 8*nG;
    df2 = N-nG;
    qtukeyR(alpha, nG  , df2, outdir);
    qtukeyR(alpha, nG-1, df2, outdir);

end