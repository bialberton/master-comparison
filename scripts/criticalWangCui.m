function [c] = criticalWangCui(alpha,nG,nS, fhcv, MCsample)
% Calculates the critical value of the first step from Wang-Cui multiple
% comparisons correction method. Adapted from Wang & Cui (2017) R's algorithm 
% to MATLAB
%                      
% Usage:
% [c] = criticalWangCui(alpha,nG,nS, fhcv, MCsample)
% 
% Inputs: 
% alpha  : significance level of the test (e.g. 0.05%)
% nG     : number of groups
% nS     : [nG x 1] array containing the sample sizes of each group
% fhcv   : critical value used in step 2 of the Fisher-Hayter correction
%          method. It is calculated as qtukeyR(alpha, nG-1, df2, datadir)/sqrt(2)
%
% Outputs:
% critValue : critical value to be used in the first step of Wang-Cui
% correction method
%
% _____________________________________
% Bianca A. V. Alberton
% UTFPR
% Nov/2019 (first version)
% 
% Adapted from Wang, B. and Cui, X. (2017), An improved uniformly more powerful 
% exact Fisher–Hayter pairwise comparisons procedure. Biom. J., 59: 767-775. 
% doi:10.1002/bimj.201500265
% 

% Creates the samples
y     = zeros(MCsample, nG);
s2    = zeros(MCsample, nG);
for i = 1 : nG
    y(:,i) = normrnd(0,1/sqrt(nS(i)),[MCsample,1]);
    s2(:,i)= chi2rnd(nS(i)-1,[MCsample, 1]);
end

% All possible pairwise comparisons
Fstat = zeros(MCsample,1);
pair = combnk(1:nG,2)';

%Calculates the stats for each Monte Carlo test
for i = 1 : MCsample
    s = sqrt(sum(s2(i,:))./sum(nS-1));
    
    Tstat = abs( y(i,pair(1,:)) - y(i,pair(2,:)) )./ sqrt( 1./nS(pair(1,:)) + 1./nS(pair(2,:)) )./s;
    if any(Tstat >= fhcv)
        Fstat(i) = getF(y(i,:),s^2,nS);
    end
end

c = quantile(Fstat,(1-alpha)); 

    function Fstat = getF(y,s2,nS)
        ybar=sum(nS.*y)./sum(nS);
        Fstat=sum(nS.*(y-ybar).^2)./s2./(length(y)-1);
    end

end