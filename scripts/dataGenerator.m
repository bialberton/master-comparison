function [Y] = dataGenerator(N1, N2, dist)
% Function that generates a matrix of N1 by N2 with random values following
% the specified distribution
%
% Usage:
% Y = dataGenerator(N1, N2, dist)
% 
% Inputs: 
% N1   : number of rows
% N2   : number of columns
% dist : distribution to be computed. 
%        Options: norm - normal distribution
%                 weib - weibull distribution
%                 lapl  - laplace distribution
%
%
% Outputs:
% Y    : an N1 by N2 matrix with the data following the distribution dist
%

if     strcmp(dist,'norm')
    Y = randn(N1,N2);
elseif strcmp(dist,'weib')
    kInv = 3; % kInv = 1/k
    lambda = 1;
    Y = (wblrnd(lambda,1/kInv,[N1,N2])-gamma(1+kInv)*lambda)./(lambda^2 * sqrt(gamma(1+2*kInv)-gamma(1+kInv)^2)); %Mean 0 and variance 1
elseif strcmp(dist,'lapl')
    Y = randlap([N1,N2]);
end