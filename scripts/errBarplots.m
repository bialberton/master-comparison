% % Parameters for testing:
testSymbol = markers(plotTest);
testColor  = colors(plotTest,:);
testLine   = linetype(plotTest);

% Get the error rates to be ploted
if withSignal
    erPloted = erT3toNG(:,plotTest)*100;
    sig = '';
else
    erPloted = erT1toNG(:,plotTest)*100;
    sig = 'out';
end

% adjust the interval to appear correctly in the errorbar
if intervalToPlot == 0
    interval = ci(:,:,plotTest)*100;
    intervalDown =  erPloted-squeeze(interval(:,1,:));
    intervalUp =  squeeze(interval(:,2,:))-erPloted;
    minYValue = floor(min(min(squeeze(interval(:,1,:)))));  
    maxYValue = floor(max(max(squeeze(interval(:,2,:)))))+1;
elseif intervalToPlot == 1
    interval = erMaxMin(:,:,plotTest)*100;
    minYValue = floor(min(min(squeeze(interval(:,1,:)))));  
    maxYValue = floor(max(max(squeeze(interval(:,2,:)))))+1;
    intervalDown =  erPloted-squeeze(interval(:,1,:));
    intervalUp =  squeeze(interval(:,2,:))-erPloted;
else
    intervalDown = erStd(:,plotTest)*100;
    intervalUp = intervalDown;
    minYValue = floor(min(min(erPloted - intervalDown)));  
    maxYValue = floor(max(max(erPloted + intervalDown)))+1;
end

if(minYValue < 0)
    minYValue = 0;
end

% Plots the errorbar
figure; hold on;
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 0.4, 0.45]);
for i = 1 : sum(plotTest(:))
    e = errorbar(nGtested, erPloted(:,i),intervalDown(:,i),intervalUp(:,i));
    e.Marker = testSymbol{i};
    e.MarkerSize = markerSize;
    e.Color  = testColor(i,:);
    e.LineStyle = testLine{i};
    e.LineWidth = lineWid;

end
legend(test(plotTest),'location', 'bestoutside');

bal = 'b';
if ~balance
    bal = 'Unb';
end

cont = '';
if subset
    cont = 'a subset of ';
end

title(sprintf('FWER %s - %salanced model with%s signal using %sall contrasts',dist, bal, sig, cont));
pHandler = gca;
pHandler.LineWidth = 1.5;
set(gca, 'FontSize',14)
%pHandler.OuterPosition = [0 0 1 0.9];

%X axis properties
xlabel('Number of groups');
set(gca,'XLimMode','manual' );
set(gca,'XLim',[nGtested(1)-1, nGtested(end)+1] );
set(gca,'XTick',[0,nGtested(1):nGtested(end)] );
set(gca,'XTickLabel',[0,nGtested(1):nGtested(end)]);

% % Y axis properties
% set(gca, 'YMinorTick','on')
% if maxYValue > 14
%     tickSpace = 2;
%     pHandler.YAxis.MinorTickValues = pHandler.YAxis.Limits(1):1:pHandler.YAxis.Limits(2);
% elseif maxYValue > 7
%     tickSpace = 1;
%     pHandler.YAxis.MinorTickValues = pHandler.YAxis.Limits(1):.5:pHandler.YAxis.Limits(2);
% else
%     tickSpace = 0.5;
%     pHandler.YAxis.MinorTickValues = pHandler.YAxis.Limits(1):.25:pHandler.YAxis.Limits(2);
% end

ylabel('Family-wise error rate (%)');
set(gca,'YLimMode','manual' );
set(gca,'YLim',[minYValue, maxYValue] );
% set(gca,'YTick',[minYValue:tickSpace:maxYValue] );
% set(gca,'YTickLabel',[minYValue:tickSpace:maxYValue]);
print(sprintf('%s/errbar_subset-%d-signal-%d-balance-%d',plotsdir,subset,withSignal,balance),'-r300','-dsvg');

fprintf(1,'Plotted graph subset-%d-signal-%d-balance-%d\n',subset,withSignal,balance);
close;