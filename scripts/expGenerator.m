function [M, Cset, G] = expGenerator(nS, nG, balanced)
% Function that creates a design matrix and a contrast set for a
% experiment to compare the means between groups. M and C are constructed
% considering the cell means model (instead of factor effect model)
%
% Usage:
% [M, Cset, G] = expGenerator(nS, nG, balanced)
% 
% Inputs: 
% nS        : number of subjects
% nG        : number of groups
% balanced  : true for a balaced model and false for a unbalanced model
%             with groups with random sizes. The minimum size of a group is
%             5              
% Outputs:
% M    : Design matrix
% Cset : Contrast matrix between all groups
% G    : vector with the number of subjects in each group
%

minNS = 4;   %Smaller number of subjects per group

if(balanced)
    if(mod(nS,nG) == 0 )
        G = nS/nG * ones(nG,1);
    else
        error('Impossible to create a balanced model with this number of subjects and groups');
    end
else
    %Defining the number of subjects in each group for unbalanced model
    G = rand(nG,1);
    G = round(G/sum(G)*nS);
    G(G <= minNS) = G(G <= minNS) + minNS;
    %Att only the 1st maximum value (sometimes there is more than one group with the biggest number of subj)
    while(max(G) <= (sum(G)-nS + minNS)) %Verify if the num. of excedent subjects needs to be subtracted from more than one group
        G(find(G == max(G),1)) = -minNS + (G(find(G == max(G),1)));
    end
    G(find(G == max(G),1)) = nS - sum(G) + (G(find(G == max(G),1)));
end 

%M (Based on the groups G) 
M = cell(nG,1);
for g=1: nG
    M{g} = ones(G(g),1);
end
M = blkdiag(M{:}); %Creates the diagonal matrix where each diagonal element is one of the matrices of M{:}

%Creates the contrast matrix
tmp1 = kron(eye(nG),ones(nG,1));
tmp2 = kron(ones(nG,1),-eye(nG));

Cset = tmp1 + tmp2;
Cset(all(~Cset,2),:) = [];
Cset = Cset';