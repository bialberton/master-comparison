%Plot the progression of test
% test     =>       Tpce., Ppce,   Pfwe,DSpa,DSpe,  LSDpa,LSDpe,  Tuk, FH, WG, SCh, Bonf
plotTest = logical([0,       0,     1,   1,  1,      1,     1,    0,   0,   0,   0,  0]);
% plotTest = logical([0,       0,     1,   1,  1,      1,     0,    1,   1,   1,   1,  1]);
markers  =          {'none', 'none', '*','+','+', 'x', 'none',  's', 'd', 'o', '^', '+'};
linetype =          {':',      '-',  '-','--', '-',  '-',   '-',  '--', '-','--', '-','-'};
colors   =  [[0.6 0.6 0.6]; ...                %T parametric pcer
    [1 1 1]; ...                %T permutation pcer
    [0.3010 0.7450 0.9330]; ... % Perm. corrected  %[0 0.4470 0.7410] | [0.6000 0.5800 1]
    [0.8500 0.3250 0.0980]; ... % Sidak para
    [0.5020 0.8706 0.4667]; ... % Sidak perm
    [1 0 0]; ...                % LSD para
    [0 0 0]; ...                % LSD perm
    [0.6400 0.3000 0.6560]; ... % Tukey
    [0.1686 0.4902 0.1686]; ... % Fisher-Hayter %[0.4660 0.6740 0.1880] | [0.2000 0.6000 0.2000]
    [0.9290 0.6940 0.1250]; ... % Wang-Cui %[0.3010 0.7450 0.9330]
    [1 0 1]; ... % Scheffe %[0.9290 0.6940 0.1250]  | [0.3010 0.7450 0.9330]
    [0 0.4470 0.7410]];                   % Bonferroni

saveFormat = 'svg';
lineWid    =  1.5;
markerSize = 8;

%To make the lines at 5%
erT1toNG(:,1)  = 0.05*ones(size(erT1toNG,1),1);
erT3toNG(:,1)  = 0.05*ones(size(erT1toNG,1),1);
ci(:,1,:)      = zeros(size(ci(:,1,:)));
erMaxMin(:,1,:)= 0.05*ones(size(erMaxMin(:,1,:)));
erStd(:,1)     = zeros(size(erStd(:,1)));

%Generates the error Bars
% intervalToPlot = 0;   %ci
% intervalToPlot = 1;   %maxMin
intervalToPlot = 2;   %stdEr

errBarplots %Plots and saves

if(withSignal)
   powerGraph 
end

%%%%% Without confidence intervals:
% testsDone = logical([1,       0,     1,   1,  0,      1,     0,    1,   1,   1,   1,  1]);
% if(~withSignal)
%     xeixo = (nGroups);
%     %ER 1 to NG
%     figure; hold on;
%     for i = 1 : numel(testsDone)
%         if(testsDone(i))
%             plot(xeixo, erT1toNG(:,i)*100, 'Marker',markers{i}, 'Color', colors(i,:), 'LineStyle',linetype{i},'LineWidth',lineWid,'MarkerSize',markerSize);
%         end
%     end
%     title(sprintf('FWER (no signal)  %02d subj per group (balanced? %d ) - dist %s',ns, balance, dist));
%     legend(test(:,testsDone));
%     xlabel("Number of groups");
%     ylabel("Error rate (%)");
%     saveas(gcf, fullfile(figsDir,sprintf('ER1toNG_%s.%s',sprintf('ortho-%d-signal-%d-balance-%d',subset,withSignal,balance),saveFormat)));
% else
%     %ER 3 to NG
%     figure; hold;
%     for i = 1 : numel(testsDone)
%         if(testsDone(i))
%             plot(xeixo, erT3toNG(:,i)*100, 'Marker',markers{i}, 'Color', colors(i,:), 'LineStyle',linetype{i},'LineWidth',lineWid,'MarkerSize',markerSize);
%         end
%     end
%     title(sprintf('FWER (data with signal) %02d subj per group (balanced? %d ) - dist %s',ns, balance, dist));
%     legend(test(:,testsDone));
%     xlabel("Number of groups");
%     ylabel("Error rate (%)");
%     saveas(gcf, fullfile(figsDir,sprintf('ER3toNG_%s.%s',sprintf('ortho-%d-signal-%d-balance-%d',subset,withSignal,balance),saveFormat)));
%   
%     
%     %POWER
%     figure; hold;
%     for i = 1 : numel(testsDone)
%         if(testsDone(i))
%             plot(xeixo, meanPower(:,i)*100, 'Marker',markers{i}, 'Color', colors(i,:), 'LineStyle',linetype{i},'LineWidth',lineWid,'MarkerSize',markerSize);
%         end
%     end
%     title(sprintf('Power with %02d subjects per group (balanced? %d )- dist %s',ns, balance, dist));
%     legend(test(:,testsDone));
%     xlabel("Number of groups");
%     ylabel("Power (%)");
%     saveas(gcf, fullfile(figsDir,sprintf('POWER_%s.%s',sprintf('ortho-%d-signal-%d-balance-%d',subset,withSignal,balance),saveFormat)));
% end
% close all;
