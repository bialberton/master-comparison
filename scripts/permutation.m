function [ppermT,ppermF, cppermT] = permutation(Y,M,Cset,F,df1,df2,nP)
% For a experiment Y = M * Cset , calculates the t and F values for a
% permutation test and the corrected value across contrasts
%
% Usage:
% [ppermT,ppermF, cppermT] = permutation(Y,M,Cset,F,df1,df2,nP)
% 
% Inputs: 
% Y        : N by V array of image data. N is the number of subjects and V
%            the number of voxels
% M        : N by nG array containing the design matrix. N is the number of
%            subjects and nG is the number of groups
% Cset     : nG by N array containing the contrast matrix. N is the number of
%            subjects and nG is the number of groups
% F        : nG by (nG-1) array with the contrasts to be used in the F test. 
%            N is the number of subjects and nG is the number of groups
% df1      : degrees of freedom due to the contrast
% df2      : degrees of freedom of the model
% nP       : number of permutation matrices to be generated
%
% Outputs:
% ppermT    : permutation p values for a t test
% ppermF    : permutation p values for a F test
% cppermT   : corrected permutation p values for a t test
%

[N V] = size(Y);
nC  = size(Cset,2);
%Permutation matrix
P = zeros(N,nP); 
P(:,1) = (1:N)';
for p = 2 : nP
   P(:,p) = randperm(N); 
end

statT  = pivotal2(Y,M,Cset,1,df2);
statF  = pivotal2(Y,M,F,df1,df2); 
ppermT = ones(nC,V);   % Permutation p value for each contrast in each voxel
ppermF = ones(1,V);    % Permutation p value of F statistic
maxT = zeros(nP,V);

for p = 2 : nP
    Mp = M(P(:,p), :); % Permutes the rows of the design matrix (subjects)
    % Calculates t-test
    statTp = pivotal2(Y,Mp,Cset,1,df2); 
    ppermT = ppermT + (statTp >= statT);
    maxT(p,:)= max(statTp,[],1);
    
    % Calculates F-test
    statFp = pivotal2(Y,Mp,F,df1,df2); 
    ppermF = ppermF + (statFp >= statF);   
end
ppermT = ppermT./nP;  
ppermF = ppermF./nP; 

%Calculates the corrected t-test fo permutation
cppermT = zeros(size(ppermT));
for v = 1 : V
   cppermT(:,v) = palm_datapval(statT(:,v),maxT(:,v),false); 
end

