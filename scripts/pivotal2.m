function [stat] = pivotal2(Y,M,C,df1,df2)
% Computes the t Test of F test of a regressor
%
% Usage:
% [pval,stat] = pivotal2(Y,M,C,df1, df2)
% 
% Inputs for a full model specification:
% Y       : A n by v matrix of observations. n is the number of subjects
%           and v is the number of voxels
% M       : A n by ng design matrix. n is the number of subjects and ng is
%           number of groups
% C       : A ng by r contrast matrix. ng is number of groups and r is
%           number of contrasts
% df1     : Degrees of freedom of the contrast matrix. Use df = 1 for F
%           statistic and df = rank(C) for F statistic
% df2     : Degrees of freedom of the model
%
% Outputs:
% stat    : A nC by V matrix of t or F scores. nC is the number of contrasts
%           and v is the number of voxels



%Regressor coefficient and residuals
psi = M\Y;
res = Y-M*psi;

if df1 == 1 %t test
    nC = size(C,2); %num of contrasts
    stat = zeros(nC,size(Y,2)); %Stats is a  nC x V matrix
    res2 = sqrt(sum(res.*res,1)/df2);
    for c = 1 : nC
        stat(c,:) = C(:,c)'* psi/sqrt(C(:,c)'*pinv(M'*M)*C(:,c))./res2;
    end
else %F test
    stat = sum((psi'*C/(C'*pinv(M'*M)*C))'.*(C'*psi))/df1./(sum(res.*res,1)/df2);
end

