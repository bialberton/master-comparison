function critValue = preComputed(alpha,nG,df2,type)
% Recovers the critical value of the correction method without using R
%                      
% Usage:
% [critValue] = statTests(Y, M, Cset,V,N,nG,nP,alpha, outdir)
% 
% Inputs: 
% alpha  : significance level of the test (e.g. 0.05%)
% nG     : number of groups
% df2    : degrees of freedom (df2 = N - rank(m) = N - nG)
% type   : correction method. Options: 
%          'tukey'- returns the value of qtukeyR(alpha, nG, df2)
%          'fh'   - returns the value of qtukeyR(alpha, nG-1, df2) => Fisher--Hayter
%
% Outputs:
% critValue : critical value to be used in the statistic
%

critValue = Inf;
if alpha == 0.05
    % Considering 10 subjects per group and nG = 3 : 15, that is, df2 = nG*10-nG
    df2_pc = [27,36,45,54,63,72,81,90,99,108,117,126,135]; 
    
    df_idx = find(df2_pc == df2);
    if isempty(df_idx)
        fprintf(1,'There is no value stored for df2 = %d',df2);
        return; 
    end
    
    if strcmp('tukey',type)
        nG_Tukey = [3:15]; 
        
        if(nG_Tukey(df_idx) ~= nG)
            fprintf(1,'There is no value stored Tukey correction with df2 = %d with %d groups',df2, nG);
            return; 
        end
        
        q = [3.50642612339269, 3.80879838143466, 4.01841708977597, 4.17826521883831, ...
            4.30713601757573, 4.41490417856310, 4.50739116407123, 4.58831282736549, ... 
            4.66018361779180, 4.72478300035334, 4.78341574162579, 4.83706649247575, ...
            4.88649647437652];
        critValue = q(df_idx);
        
    elseif strcmp('fh',type)
        nG_Hayter = [2:14]; % nG_Tukey -1 
        
        if(nG_Hayter(df_idx) ~= (nG-1))
            fprintf(1,'There is no value stored Fisher-Hayter correction with df2 = %d with %d groups',df2, nG);
            return; 
        end
        
        q = [2.90172654411936, 3.45675810925593, 3.77269677855799, 3.99102366835272, ...
            4.15670729198499, 4.28967284954906, 4.40042891419630, 4.49516748496807, ...
            4.57783155911210, 4.65108098328231, 4.71679175338628, 4.77633483369764, ...
            4.83074171610261];
        critValue = q(df_idx);
    else
        displ('Error - type of critical value not found (options = tukey or fh)');
    end

else
    displ('Error - Pre-calculated critical value different from the database (alpha = 0.05)');
end


%%%%%To generate the values:
%%%% Tukey
% q = zeros(size(nG_Tukey));       
% disp('Tukey critical values')
% for i = 1 : size(df2_pc,2)
%     q(i) = qtukeyR(alpha, nG_Tukey(i)  , df2_pc(i), 'results/tmp');
% end
% disp(q)

%%%% Fisher--Hayter
% q = zeros(size(nG_Hayter));       
% disp('Fisher-Hayter critical values')
% for i = 1 : size(df2_pc,2)
%     q(i) = qtukeyR(alpha, nG_Hayter(i)  , df2_pc(i), 'results/tmp');
% end
% disp(q)