function [pVal] = ptukeyR(q,k,df)
% Calls a R script to calculate the p values of the studentized range distribution
%
% Usage:
% pVal = ptukeyR(q,k,df)
% 
% Inputs: 
% q   : statistic to be computed
% k   : number of groups (for Tukey's method) ou number of groups -1 (for 
%       Hayter's method) 
% df  : degrees of freedom of the model
%
% Outputs:
% pVal : p values of the studentized range distribution
%

%Stores data in a csv file
[w,h] = size(q);
if(h == 1)
    data = [k df w h q'];
else
    data = [k df w h zeros(1,h-4)];
    data = [data; q];
end
dlmwrite('mat.csv', data, 'precision',20);
%Calls R script
system([pwd '/tukeyR.R']);
%Get's the p values from another csv file
pVal = load('r.csv');

