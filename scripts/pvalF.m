function pparaF = pvalF(statF, df1, df2)
% Calculates the p values of a parametric F test using a beta distribution.
%
% Usage:
% pparaF = pvalF(statF, df1, df2)
% 
% Inputs: 
% statF : Array of 1 by n statistics for which the p value will be computed
% df1   : degrees of freedom due to the contrast
% df2   : degrees of freedom of the model
%
% Outputs:
% pparaF : p values of the parametric F test
%

% p-value of F statistic for the actual model: uses Beta distribution
% instead of (1 - fcdf), so that we don't lose really small p-values
df2F   = ones(size(statF))*df2;
B      = (df1.*statF./df2F)./(1+df1.*statF./df2F);
pparaF = betainc(1-B,df2F/2,df1/2);