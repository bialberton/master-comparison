#!/usr/bin/env Rscript

args <- commandArgs();
filename = args[6];
mData <- read.csv(filename,header=FALSE)
m = as.matrix(mData); #Transform it to matrix
#Extract the data we need
k <- m[1,1];
df <- m[1,2];
w <- m[1,3];
h <- m[1,4];

if(h == 1)
{	
	p = m[5 :(w+4)];
} else
	p = m[2:(w+1),];



qStat = qtukey(p,k,df, lower.tail = FALSE);

#Capture and dump to out file
data <- data.frame(qStat);
outFilename = gsub(".csv","_r.csv",filename);
write.table(format(qStat, nsmall=20),outFilename, row.names = FALSE, col.names = FALSE, quote=FALSE, eol="\r");
