function [qStat] = qtukeyR(p,k,df,outdir)
% Calls a R script to calculate the statistic values of the studentized range distribution
%
% Usage:
% qStat = qtukeyR(p,k,df)
% 
% Inputs: 
% p         : p-value / significance level of the test
% k         : number of groups (for Tukey's method) ou number of groups -1 (for 
%             Hayter's method) 
% df        : degrees of freedom of the model
% outdir    : name of the folder where the data will be exchanged between
%             matlab and R
%
% Outputs:
% qStat : statistic corresponding to the studentized range distribution at
% a p level of significance
%
csvFile = sprintf('data_qtukey_%.2falpha_%03dgroups_%04ddf.csv',p,k,df);
rFile = sprintf('data_qtukey_%.2falpha_%03dgroups_%04ddf_r.csv',p,k,df);

if(~exist(fullfile(outdir,rFile),'file'))
    %Stores data in a csv file
    [w,h] = size(p);
    if(h == 1)
        data = [k df w h p'];
    else
        data = [k df w h zeros(1,h-4)];
        data = [data; p];
    end

    dlmwrite(fullfile(outdir,csvFile), data, 'precision',20);
    %Calls R script
    command = [fullfile(pwd,'qtukeyR.R '), fullfile(outdir, csvFile)];
    system(command);

end

%Get's the statistic values from another csv file
qStat = load(fullfile(outdir,rFile));

