%Reproduzing data from Seaman's article (Seaman, M. A.; Levin, J. R.; Serlin, R. C.; 1991)
clear all; close all;

nP = 5000;
alpha = 0.05;
balanced = true;
dist = 'norm';
outdir = fullfile(pwd,'results','test');

nTests = 5;
erData = zeros(nTests,9);

V  = 5000; 
nG = 5
ns = 15;
N = ns * nG;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Null case - Table 4 %%%%%%%%%%%%%%%%%%%%
Y = dataGenerator(N,V,dist);
[M, Cset, nS] = expGenerator(N, nG, balanced);

[erT1toNG, ~, ~, testName ] = statTests(Y,M,Cset,V,N,nG,nP,alpha, outdir, false(1,size(Cset,2)));
erData(1,:) = erT1toNG;


%%%%%%%%%%%%%% Adding 80% power signal - Table 5 - reading file %%%%%%%%%%%%%%%
%Adding signal
Y2 = Y;
Y2(1:2*ns,:) = Y2(1:2*ns,:) + 0.8560;

% Defining which contrasts have signal
% Without signal: 1-2, 2-1, 3-4, 4-3, 3-5, 5-3, 4-5, 5-4, 3-1, 4-1, 5-1, 3-2, 4-2, 5-2 
% With signal: 1-3, 1-4, 1-5, 2-3, 2-4, 2-5

% mean 
% group:    1  2  3  4  5 
% Csignal = [ 1  0  -1 0  0; ...
%             1  0  0 -1  0; ...
%             1  0  0  0 -1; ...
%             0  1  -1 0  0; ...
%             0  1  0 -1  0; ...
%             0  1  0  0 -1];

Csignal = [ 1  0  -1 0  0; ...
            1  0  0 -1  0; ...
            1  0  0  0 -1; ...
            0  1  -1 0  0; ...
            0  1  0 -1  0; ...
            0  1  0  0 -1];
        
CsignalIdx = false(1,size(Cset,2));

for i = 1 : size(Csignal,1)
    idx = ismember (Cset', Csignal(i,:), 'rows')';
    CsignalIdx = CsignalIdx | idx;
end

%Calculate everything
[erT1toNG, erT3toNG, ~, ~ ] = statTests(Y2,M,Cset,V,N,nG,nP,alpha, outdir, CsignalIdx);
erData(2,:) = erT3toNG;


%%%%%%%%%%%%%%%% Plots %%%%%%%%%%%%%%%%%%%%
tableTitle = {"Test        ","Table 4", "Table 5"};

fid = 1;
aux = zeros(size(erT1toNG));
fprintf(fid, "| %s | %s | %s | \n",tableTitle{:} );
fprintf(fid,"\n");

for j = 1 : size(testName,2)
    fprintf(fid, '| %s |', testName{j});
    fprintf(fid, ' %2.4f  | %2.4f  | \n',  erData(1, j)*100, erData(2, j)*100);
end
fprintf(fid,"\n");

% 
% %%%%%%%%%%%%%%%%%%%%%%%%%% Adding 80% power signal - Table 5a %%%%%%%%%%%%%%%%%%%%
% [N, V]  = size(Y);
% df1     = nG-1;
% df2     = N-rank(M);
% effi    = sqrt(Cset(:,1)'*pinv(M'*M)*Cset(:,1));       %efficiency
% a       = 1.43*finv(1-alpha,df1,df2) * effi/2;  %signal factor
% 
% Y2 = Y;
% %Adding signal to the matrix
% Y2(1:nS(1),:)                 = Y2(1:nS(1),:)+ a*ones(nS(1),V);                  %add signal to the 1st group
% Y2((nS(1)+1):(nS(1)+nS(2)),:) = Y2((nS(1)+1):(nS(1)+nS(2)),:) - a*ones(nS(2),V); %subtract the signal to the 2nd group
% 
% 
% [~, erT3toNG, ~, ~ ] = statTests(Y2,M,Cset,V,N,nG,nP,alpha, outdir, false(1,size(Cset,2)));
% erData(2,:) = erT3toNG;
