function [erT1toNG, erT3toNG, powerMeth, testName] = statTests(Y, M, Cset,V,N,nS,nG,nP,alpha, datadir,  outdir, CpowerIdx, MCsample)
% Function that performs the following statistical tests over the data
% received: 
%   - Parametric tests: t and F test without correction, Fisher's LSD,
%                       Sidak, Tukey, Hayter, Scheffe and Wang-Cui
%   - Permutation test for F test and t test with and without correction. Fisher's LSD,
%                       Sidak, Tukey and Hayter are also caluclated in
%                       relation to permutation's t test without correction
%                      
% Usage:
% [erT1toNG, erT3toNG, power, testName] = statTests(Y, M, Cset,V,N,nG,nP,alpha, outdir)
% 
% Inputs: 
% Y      : n by v matrix with noise data, where n is the number of subjects
%          and v the number of voxels.
% M      : n by nG matrix with the design matrix
% Cset   : nG by nC matrix with the contrasts, where nG is the number of
%          groups and nC is the the number of contrasts
% V      : number of voxels
% N      : number of subjects
% nS     : number of subjects per group (necessary to calculate Wang-Cui)
% nG     : number of groups
% nP     : number of permutations of the permutation test
% alpha  : significance level of the test (e.g. 0.05%)
% outdir : folder where the files used to calculate the q threshold
%          will be stored
% MCsample   : number of monte Carlo Samples used in the Wang-Cui method
%
% Outputs:
% erT1toNG : error considering all contrasts
% erT3toNG : error of all contrasts except those which have signal in it 
%            (all contrasts related to gorups 1 and 2)
% power    : file with the power of test (calculated in relation to the first 
%            contrast)
% testName : array with the name of each contrast in the same orther used
%            in erT1toNG, erT3toNG and power
%

nC  = size(Cset,2);
F = Cset(:,1:(nG-1));
df1 = rank(F);
df2 = N-rank(M);

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Tests %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
statT = pivotal2(Y,M,Cset,1,df2);
statF = pivotal2(Y,M,F,df1,df2);

%t and F test without correction
uparaT = tinv(1-alpha,df2);
uparaF = finv(1-alpha,df1, df2);

%Mask after F test - used in Fisher's LSD and Hayter's procedure %% AND Scheffe
paraMaskFtest = statF >= uparaF;
statParaFtest = (-Inf) * ones(size(statT));
statParaFtest(:,paraMaskFtest) = statT(:,paraMaskFtest);

%Sidak para
quantileAdjSidak = (1-alpha).^(1/nC);
uSidak    = tinv(quantileAdjSidak,df2);

%Tukey and Hayter - Using pre-computed values for faster calculation
uTukey  = preComputed(alpha,nG,df2,'tukey')/sqrt(2);
uHayter = preComputed(alpha,nG,df2,'fh')/sqrt(2);

% uTukey  = qtukeyR(alpha, nG  , df2, datadir)/sqrt(2);
% uHayter = qtukeyR(alpha, nG-1, df2, datadir)/sqrt(2);
% fprintf(1,'Pre-computed Tukey %0.5f , and obtained from R %0.5f\n', uTukey,  qtukeyR(alpha, nG  , df2, datadir)/sqrt(2))
% fprintf(1,'Pre-computed Hayter %0.5f , and obtained from R %0.5f\n',uHayter, qtukeyR(alpha, nG-1 , df2, datadir)/sqrt(2))

%Permutation
[ppermT,ppermF,cppermT] = permutation(Y,M,Cset,F,df1,df2,nP);

% Mask after F test - used in Fisher's LSD (permutation)
permMaskFtest = ppermF <= alpha;
% sizePermFtest = sum(permMaskFtest);
ppermFtest = ones(size(ppermT));
ppermFtest(:,permMaskFtest) = ppermT(:,permMaskFtest);

%Sidak perm
ppermSidak = ones(nC,V) - (ones(nC,V)-ppermT).^nC;

%%%% Methods added in v2 of the paper
%Scheffe 
uScheffe = sqrt((nG-1)*finv(1-alpha,nG-1, df2));

%Mask used in Wang-Cui method
cWangCui = criticalWangCui(alpha,nG,nS', uHayter, MCsample);
paraMaskWC = statF >= cWangCui;
statWCFtest = (-Inf) * ones(size(statT));
statWCFtest(:,paraMaskWC) = statT(:,paraMaskWC);

%Bonferroni
quantileAdjBonferroni = 1-alpha/nC;
uBonferroni    = tinv(quantileAdjBonferroni,df2);

%%%%%%%%%%%%%%%%%%%%% Type I error %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Number of tests per design matrix
nT = V * nC;
CnoSignalIdx = ~CpowerIdx;  
nCsubset     = sum(CnoSignalIdx)*V;      %Number of tests after excluding the contrasts that envolve groups 1 and 2

%t and F test without correction
pparaT1toNG_pcer = sum(sum(statT                 >= uparaT ))/nT;
pparaT3toNG_pcer = sum(sum(statT(CnoSignalIdx,:) >= uparaT ))/nCsubset;
%pparaF1to15_pcer = sum(statF >= uparaF)/V;

%Sidak parametric
paraSidak1toNG_fwer = sum(any(statT                 >= uSidak, 1),2)/V;
paraSidak3toNG_fwer = sum(any(statT(CnoSignalIdx,:) >= uSidak, 1),2)/V;

%LSD parametric - using the F test to make a mask
paraLSD1toNG_fwer = sum(any(statParaFtest                  >= uparaT,1),2)/V;
paraLSD3toNG_fwer = sum(any(statParaFtest(CnoSignalIdx,:)  >= uparaT,1),2)/V;

%Studentized Q - Tukey
tukey1toNG_fwer = sum(any(statT                 >= uTukey, 1),2)/V;
tukey3toNG_fwer = sum(any(statT(CnoSignalIdx,:) >= uTukey, 1),2)/V;

%Studentized Q - Hayter
hayter1toNG_fwer = sum(any(statParaFtest                 >= uHayter, 1),2)/V;
hayter3toNG_fwer = sum(any(statParaFtest(CnoSignalIdx,:) >= uHayter, 1),2)/V;

%Permutation t and F
ppermT1toNG_pcer = sum(sum(ppermT <= alpha ))/nT;
ppermT3toNG_pcer = sum(sum(ppermT(CnoSignalIdx,:) <= alpha ))/nCsubset;
%ppermF1to15_pcer = sum(ppermF <= alpha)/V;

%Permutation corrected
cppermT1toNG_fwer = sum(any(cppermT                 <= alpha, 1),2)/V;
cppermT3toNG_fwer = sum(any(cppermT(CnoSignalIdx,:) <= alpha, 1),2)/V;

% if(~withSignal & cppermT1toNG_fwer >=alpha)
%    disp('stop') 
% end

%Sidak permutation
permSidak1toNG_fwer = sum(any(ppermSidak                 <= alpha,1),2)/V;
permSidak3toNG_fwer = sum(any(ppermSidak(CnoSignalIdx,:) <= alpha,1),2)/V;

%LSD permutation - using the F test to make a mask
permLSD1toNG_fwer = sum(any(ppermFtest                 <= alpha ,1),2)/V;
permLSD3toNG_fwer = sum(any(ppermFtest(CnoSignalIdx,:) <= alpha ,1),2)/V;

%%%% Methods added in v2 of the paper
% Scheffé 
scheffe1toNG_fwernoF = sum(any(statT                 >= uScheffe, 1),2)/V;
scheffe3toNG_fwernoF = sum(any(statT(CnoSignalIdx,:) >= uScheffe, 1),2)/V;

% Wang-Cui method
wangCui1toNG_fwer = sum(any(statWCFtest                  >= uHayter,1),2)/V;
wangCui3toNG_fwer = sum(any(statWCFtest(CnoSignalIdx,:)  >= uHayter,1),2)/V;

%Bonferroni
paraBonf1toNG_fwer = sum(any(statT                 >= uBonferroni, 1),2)/V;
paraBonf3toNG_fwer = sum(any(statT(CnoSignalIdx,:) >= uBonferroni, 1),2)/V;

%%%%%%%%%%%%%%%%%%%%% Power %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pwrParaT_pcer     = mean(statT(1,:)                    >= uparaT, 2);      %Parametric T
pwrParaSidak_fwer = sum(any(statT(CpowerIdx,:)         >= uSidak, 1),2)/V; %Sidak applied to param. T
pwrParaLSD_fwer   = sum(any(statParaFtest(CpowerIdx,:) >= uparaT, 1),2)/V; %LSD applied to param. T
pwrTukey_fwer     = sum(any(statT(CpowerIdx,:)         >= uTukey, 1),2)/V; %Tukey
pwrHayter_fwer    = sum(any(statParaFtest(CpowerIdx,:) >= uHayter,1),2)/V; %Hayter

pwrPermT_pcer     = mean(ppermT(1,:)                  <= alpha,2);       %Permutation T
pwrCppermT_fwer   = sum(any(cppermT(CpowerIdx,:)      <= alpha, 1),2)/V; %Permutation T corrected
pwrPermSidak_fwer = sum(any(ppermSidak(CpowerIdx,:)   <= alpha, 1),2)/V; %Sidak applied to perm. T
pwrPermLSD_fwer   = sum(any(ppermFtest(CpowerIdx,:)   <= alpha, 1),2)/V; %LSD applied to perm. T

%%%% Methods added in v2 of the paper
pwrScheffe_fwernoF   = sum(any(statT(CpowerIdx,:)        >= uScheffe,1),2)/V; %Scheffe
pwrWangCui_fwer      = sum(any(statWCFtest(CpowerIdx,:)  >= uHayter,1),2)/V; % Wang-cui
pwrParaBonf_fwer     = sum(any(statT(CpowerIdx,:)        >= uBonferroni, 1),2)/V; %Bonferroni applied to param. T

%%%%%%%%%%%%%%%%%%%%% Prints/Output %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
testName     =   {"T para.     ",   "T perm      ",  "T perm. Cor.", "Sidak para. ",       "Sidak perm. ",        "LSD para.   ",     "LSD perm.   ",  "Tukey       ", "Hayter      ", "Wang-Cui","Scheffe-no F", "Bonferroni"};
% typeError= {   "PCER",          "PCER",            "FWER",             "FWER",             "FWER",             "FWER",             "FWER",          "FWER",         "FWER"};
powerMeth= [pwrParaT_pcer,    pwrPermT_pcer,    pwrCppermT_fwer,   pwrParaSidak_fwer,   pwrPermSidak_fwer,    pwrParaLSD_fwer,    pwrPermLSD_fwer, pwrTukey_fwer,   pwrHayter_fwer, pwrWangCui_fwer, pwrScheffe_fwernoF,pwrParaBonf_fwer];
erT1toNG = [pparaT1toNG_pcer, ppermT1toNG_pcer, cppermT1toNG_fwer, paraSidak1toNG_fwer, permSidak1toNG_fwer, paraLSD1toNG_fwer, permLSD1toNG_fwer, tukey1toNG_fwer, hayter1toNG_fwer, wangCui1toNG_fwer,scheffe1toNG_fwernoF,paraBonf1toNG_fwer];
erT3toNG = [pparaT3toNG_pcer, ppermT3toNG_pcer, cppermT3toNG_fwer, paraSidak3toNG_fwer, permSidak3toNG_fwer, paraLSD3toNG_fwer, permLSD3toNG_fwer, tukey3toNG_fwer, hayter3toNG_fwer, wangCui3toNG_fwer,scheffe3toNG_fwernoF,paraBonf3toNG_fwer];
