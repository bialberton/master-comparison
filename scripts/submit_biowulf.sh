#!/bin/bash

rootdir=/home/winkleram/repos/master-comparison.git/ContrastCorrection/
swarmfile=${rootdir}/../slurm/corrcon.swarm
alpha=0.05
V=2000
nP=5000
MCsample=5000
numRuns=5000
dist=norm


rm ${swarmfile}
for cOrthogonal in false true ; do
for balance     in false true ; do
for withSignal  in false true ; do
outdir=${rootdir}/../results/norm_dist/ortho-${cOrthogonal}_balance-${balance}_signal-${withSignal}
echo $outdir
mkdir -p ${outdir}
for nG in 3 4 5 7 9 ; do
N=$(echo "10 ${nG} * p"|dc)
for ((seed=1; seed<=${numRuns}; seed++)) ; do
echo "let \"rnd = ${RANDOM} % 600\" && sleep \${rnd} && module load octave R ; /usr/local/apps/octave/4.4.0/bin/octave --eval \"pkg load statistics; addpath('${rootdir}'); corrcon_func(${seed}, '${outdir}', '${outdir}', ${withSignal}, ${V}, ${N}, ${nG}, ${nP}, ${alpha}, '${dist}', ${balance}, ${cOrthogonal}, ${MCsample}); exit\" " >> ${swarmfile}
done
done
done
done
done

#swarm -f ${swarmfile} --gb-per-process 1 --threads-per-process 1 --processes-per-subjob 1 --logdir ${rootdir}/slurm/ --time 24:00:00 --job-name corrcon


cd ${rootdir}/../slurm
split -d -a 4 -l 1000 ${swarmfile} s.
nfiles=$(ls s.???? |wc -l)

i=0
while [[ ${i} -lt ${nfiles} ]] ; do
  if [[ $(squeue -u winkleram | grep ccon | wc -l) -eq 0 ]] ; then
    s=$(printf s.%04d ${i})
    echo "Submitting $s"
    swarm -f ${s} --gb-per-process 1 --threads-per-process 1 --processes-per-subjob 1 --logdir ${rootdir}/../slurm/ --time 24:00:00 --job-name ccon.${i}
    i=$(expr ${i} + 1)
  else
    echo "Sleeping"
    sleep 180
  fi
done


