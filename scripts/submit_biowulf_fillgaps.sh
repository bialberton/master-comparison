#!/bin/bash

rootdir=/home/winkleram/repos/master-comparison.git/
swarmfile=${rootdir}/slurm/corrcon.swarm
alpha=0.05
V=2000
nP=5000
numRuns=5000
dist=norm
run=run02

if false ; then
rm ${swarmfile}
for cOrthogonal in false true ; do
for balance     in false true ; do
for withSignal  in false true ; do
outdir=${rootdir}/results/${run}/ortho-${cOrthogonal}_balance-${balance}_signal-${withSignal}
echo $outdir
mkdir -p ${outdir}
for nG in 5 10 15 ; do
N=$(echo "8 ${nG} * p"|dc)
for ((seed=1; seed<=${numRuns}; seed++)) ; do
seedstr=$(printf "%05d" ${seed})
nGstr=$(printf "%03d" ${nG})
if [[ "$(ls ${rootdir}/results/${run}/ortho-${cOrthogonal}_balance-${balance}_signal-${withSignal}/*_${nGstr}groups_seed${seedstr}.csv|wc -l)" -ne "3" ]] ; then
echo "let \"rnd = ${RANDOM} % 300\" && sleep \${rnd} && module load octave R ; octave --eval \"pkg load statistics; addpath('${rootdir}/scripts/'); corrcon_func(${seed}, '${rootdir}/data/', '${outdir}', ${withSignal}, ${V}, ${N}, ${nG}, ${nP}, ${alpha}, '${dist}', ${balance}, ${cOrthogonal}); exit\" " >> ${swarmfile}
fi
done
done
done
done
done
fi
#swarm -f ${swarmfile} --gb-per-process 1 --threads-per-process 1 --processes-per-subjob 1 --logdir ${rootdir}/slurm/ --time 24:00:00 --job-name corrcon


cd ${rootdir}/slurm
split -d -a 4 -l 1000 ${swarmfile} s.
nfiles=$(ls s.???? |wc -l)

i=0
while [[ ${i} -lt ${nfiles} ]] ; do
  if [[ $(squeue -u winkleram | grep ccon | wc -l) -eq 0 ]] ; then
    s=$(printf s.%04d ${i})
    echo "Submitting $s"
    swarm -f ${s} --gb-per-process 1 --threads-per-process 1 --processes-per-subjob 1 --logdir ${rootdir}/slurm/ --time 24:00:00 --job-name ccon.${i}
    i=$(expr ${i} + 1)
  else
    echo "Sleeping"
    sleep 180
  fi
done


