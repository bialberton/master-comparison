clear all; close all;
addpath('../'); %Adds git parent folder

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Negative correlations in the data %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nRuns = 10000;  % Number of simulations
alpha = 0.05;  % Test level
N     = 100;   % Number of subjects
V     = 2;     % Number of voxels in the image
nM    = 2;     % Number of regressors

%Critical values
dsYfwer = 0; bonfYfwer = 0; %To compute the FWER when the data has negative correlations
dsMfwer = 0; bonfMfwer = 0; %To compute the FWER when the design has negative correlations
dsCfwer = 0; bonfCfwer = 0; %To compute the FWER when the contrasts are negative correlated

% Uses cholesky decomposition to ensure that the data has the desired correlation
Rho = -0.9999*ones(V) + 1.9999*eye(V);
L = chol(Rho); 

for run = 1 : nRuns
    Y = randn(N,V);   
    M = randn(N,nM);
 
    Y = Y - mean(Y,1);
    Y1 = Y * L; %Adds negative correlation to the data
    M1 = [ones(N,1),M(:,1)]; %add the intercept and uses only one regressor
    C = [0, 1]';
    
    [dsSignif,bonfSignif] = applyCorrection(Y1,M1,C,alpha,V);
    dsYfwer = dsYfwer + dsSignif;
    bonfYfwer = bonfYfwer + bonfSignif;
    
    Y2 = Y(:,1); %Uses only one voxel to perform the tests
    M2 = M*L; % Adds negative correlation to the design matrix
    M2 = [ones(N,1),M2]; %add the intercept
    C = [zeros(nM,1), eye(nM)]';

    [dsSignif,bonfSignif] = applyCorrection(Y2,M2,C,alpha,nM);
    dsMfwer = dsMfwer + dsSignif;
    bonfMfwer = bonfMfwer + bonfSignif;
    
    M3 = [ones(N,1),M];
    C = [0 1 -1; 0 -1 1]';
    [dsSignif,bonfSignif] = applyCorrection(Y2,M3,C,alpha,nM);
    dsCfwer = dsCfwer + dsSignif;
    bonfCfwer = bonfCfwer + bonfSignif;
    
end
dsYfwer = dsYfwer/nRuns;
bonfYfwer = bonfYfwer/nRuns;
fprintf(1,'Test 01: negative correlated data\n')
fprintf(1,'FWER  DS: %0.2f%% \tBonf: %0.2f%%\n\n',dsYfwer*100, bonfYfwer*100);

dsMfwer = dsMfwer/nRuns;
bonfMfwer = bonfMfwer/nRuns;
fprintf(1,'Test 02: negative correlated design matrix\n')
fprintf(1,'FWER  DS: %0.2f%% \tBonf: %0.2f%%\n\n',dsMfwer*100, bonfMfwer*100);

dsCfwer = dsCfwer/nRuns;
bonfCfwer = bonfCfwer/nRuns;
fprintf(1,'Test 03: negative correlated contrast matrix\n')
fprintf(1,'FWER  DS: %0.2f%% \tBonf: %0.2f%%\n\n',dsCfwer*100, bonfCfwer*100);

%%%%%%%%%%%%%%% Function to apply the correction %%%%%%%%%%%%%%
function [dsSignif,bonfSignif] = applyCorrection(Y,M,C,alpha,nTests)

[N, V] = size(Y);
nC = size(C,2);

df2 = N-rank(M);
psi = M\Y;
res = Y-M*psi;
res2 = sqrt(sum(res.*res,1)/df2);
stat = zeros(nC,size(Y,2)); %Stats is a  nC x V matrix
for c = 1 : nC
        stat(c,:) = C(:,c)'* psi/sqrt(C(:,c)'*pinv(M'*M)*C(:,c))./res2;
end
%Sidak
qSidak   = (1-alpha).^(1/nTests); % Corrected alpha
uSidak   = tinv(qSidak,df2); % Threshold to apply in the statistic
dsSignif = any(stat >= uSidak); % Significant values in the family

%Bonferroni
qBonf = 1-alpha/nTests;      % Corrected alpha
uBonf= tinv(qBonf,df2);
bonfSignif = any(stat >= uBonf);
end