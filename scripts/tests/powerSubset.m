clear all; close all;
addpath('../');

%%% Tests the power of the subset of li contrasts

%Parameters
alpha       = 0.05;      % test level
V           = 2000;      % num of voxels
groups      = [4,5,7,9]; % num of groups
dist        = 'norm';    
balance     = true;     

nRuns = 100;
pwrParaT_pcer = zeros(1,nRuns);
pwrParaT_pcer_any = zeros(1,nRuns);

for nG = groups
    for run = 1 : nRuns      
        %%%%%%% Experiment Settings %%%%%%%%%%%%%%%%%%%
        N           = nG*10;
        [M, Cset, nS] = expGenerator(N, nG, balance);
        
        %Y
        Y = dataGenerator(N,V,dist);
        Y = addSignal(Y, M, Cset, nS, alpha,dist);       
        Cset   = Cset(:,((nG-1)*2+1):(nG-1)*3);

        df2 = N-rank(M);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%% Tests %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        statT = pivotal2(Y,M,Cset,1,df2);
        uparaT = tinv(1-alpha,df2);
        
        pwrParaT_pcer(run)       = sum(statT(2,:) >= uparaT, 2)/V;
        pwrParaT_pcer_any(run)= sum(any(statT(1:2,:) >= uparaT, 1),2)/V;      %Parametric T
    end
    
    fprintf(1,'Power PCER subset parametric with %d groups: %0.4f %%\t', nG, mean(pwrParaT_pcer)*100);
    fprintf(1,'Power PCER subset any (contrasts involving groups 1 and 2): %0.4f %%\n', mean(pwrParaT_pcer_any)*100);
end
