clear all; close all
%%% Script to test the equivalence between testing using critical values and
% adjusting the p-values using Scheffé's method

addpath('../')
%Parameters
alpha       = 0.05;      % test level
V           = 2000;      % num of voxels
nG          = 3;         % num of groups
N           = nG*10;
dist        = 'norm';    % Options: 'norm','weib' and 'lapl'
balance     = false;
%%%%%%% Experiment Settings %%%%%%%%%%%%%%%%%%%
[M, Cset, nS] = expGenerator(N, nG, balance);
Y = dataGenerator(N,V,dist);
Y = addSignal(Y, M, Cset, nS, alpha,dist);

F = Cset(:,1:(nG-1));
df1 = rank(F);
df2 = N-rank(M);
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Tests %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
statT = pivotal2(Y,M,Cset,1,df2);
statF = pivotal2(Y,M,F,df1,df2);

%Critical value Scheffe 
uS2 = (nG-1)*finv(1-alpha,nG-1, df2);
uS = sqrt(uS2);
% Significant tests using critical values
scheffe_cv_statT = statT >= uS;

% Significant tests using the p-value adjustment
negValues = statT <=0;
statT2 = statT.^2/(nG-1);
statT2(negValues) = -statT2(negValues);
pScheffeT = 1-fcdf(statT2,(nG-1),df2);
scheffe_p_statT = pScheffeT <= alpha;

fprintf(1,'Number of matching  results using t-cv and t-p-value %d \tTotal tests: %d\n',sum(sum(scheffe_cv_statT == scheffe_p_statT)),numel(statT))
