clear all; close all;
addpath('../');
%%% Script to verify add a signal to groups 1 and 2  with specific power to 
%%% the data

%Parameters
alpha       = 0.05;      % test level
V           = 2000;      % num of voxels
groups      = [4,5,7,9]; % num of groups
dist        = 'norm';    
balance     = true;

power = 0.5; % Change this to see signals with different power being added
nRuns = 100;
pwr_pcer = zeros(1,nRuns);
pwrParaT_pcer_subset = zeros(1,nRuns);

fprintf(1,'Power before correction\n');
for nG = groups
    
    for run = 1 : nRuns
        
        %%%%%%% Experiment Settings %%%%%%%%%%%%%%%%%%%
        N           = nG*10;
        [M, C, nS] = expGenerator(N, nG, balance);
        Y = dataGenerator(N,V,dist);
        df2 = N-rank(M);
        
        %%% Add the signal
        % place where the center of alternative distribution will be inserted in
        % relation to the null distribution
        t_power = tinv(1-alpha,df2) + tinv(power,df2); %t-stat
        
        effi    = sqrt(C(:,1)'*pinv(M'*M)*C(:,1));       %efficiency
        signal  = t_power * effi/2;  %signal 
        
        %Adding signal to the matrix
        Y(1:nS(1),:)                 = Y(1:nS(1),:)+ signal*ones(nS(1),V);                  %add signal to the 1st group
        Y((nS(1)+1):(nS(1)+nS(2)),:) = Y((nS(1)+1):(nS(1)+nS(2)),:) - signal*ones(nS(2),V); %subtract the signal to the 2nd group
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%% Tests %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        statT = pivotal2(Y,M,C,1,df2);
        uparaT = tinv(1-alpha,df2);
        pwr_pcer(run) = sum(statT(1,:) >= uparaT, 2)/V;
    end
    fprintf(1,'%d groups, %d subjects: %0.4f %%\n', nG, N, 5mean(pwr_pcer)*100);
end

